'use strict';

const Thing = require('../thing');
const State = require('../common/state');
const CookingState = require('./cooking-state');

module.exports = Thing.mixin(Parent => class extends Parent.with(State, CookingState) {

	static get capability() {
		return 'autonomous-cooking';
	}

	static availableAPI(builder) {
		builder.action('cook')
			.description('Start cooking')
			.done();

		builder.action('stop')
			.description('Stop cooking')
			.done();
	}

	cook() {
		try {
			return Promise.resolve(this.activateCooking())
				.then(() => null);
		} catch(ex) {
			return Promise.reject(ex);
		}
	}

	stop() {
		try {
			return Promise.resolve(this.deactivateCooking())
				.then(() => null);
		} catch(ex) {
			return Promise.reject(ex);
		}
	}

	activateCooking() {
		throw new Error('activateCooking not implemented');
	}

	deactivateCooking() {
		throw new Error('deactivateCooking not implemented');
	}
});
