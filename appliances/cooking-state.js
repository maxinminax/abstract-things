'use strict';

const Thing = require('../thing');
const State = require('../common/state');
const { boolean } = require('../values');

module.exports = Thing.mixin(Parent => class extends Parent.with(State) {

	static get capability() {
		return 'cooking-state';
	}

	static availableAPI(builder) {
		builder.state('cooking')
			.type('boolean')
			.description('If the thing is currently cooking')
			.done();

		builder.event('cookingChanged')
			.type('boolean')
			.description('Cooking state of the thing has changed')
			.done();

		builder.event('cookingStarted')
			.description('Cooking has started')
			.done();

		builder.event('cookingDone')
			.description('Cooking is done')
			.done();

		builder.event('cookingStoppped')
			.description('Cooking has stopped')
			.done();

		builder.action('cooking')
			.description('Get the cooking state')
			.returns('boolean', 'If thing is currently cooking')
			.done();
	}

	constructor(...args) {
		super(...args);

		this.updateState('cooking', false);
	}

	/**
	 * Get if thing is cooking.
	 */
	cooking() {
		return Promise.resolve(this.getState('cooking'));
	}

	updateCooking(cooking) {
		cooking = boolean(cooking);

		if(cooking) {
			// Assume error state has been cleared
			this.updateError(null);
		}

		if(this.updateState('cooking', cooking)) {
			this.emitEvent('cookingChanged', cooking);

			if(cooking) {
				// Emit the cooking started event
				this.emitEvent('cookingStarted');
			} else {
				this.emitEvent('cookingStopped');
			}
		}
	}
});
