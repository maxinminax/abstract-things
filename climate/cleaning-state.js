'use strict';

const Thing = require('../thing');
const State = require('../common/state');
const { boolean } = require('../values');

module.exports = Thing.mixin(Parent => class extends Parent.with(State) {

	static get capability() {
		return 'cleaning-state';
	}

	static availableAPI(builder) {
		builder.state('cleaning')
			.type('boolean')
			.description('If the thing is currently cleaning')
			.done();

		builder.event('cleaningChanged')
			.type('boolean')
			.description('Cleaning state of the thing has changed')
			.done();

		builder.event('cleaningStarted')
			.description('Cleaning has started')
			.done();

		builder.event('cleaningDone')
			.description('Cleaning is done')
			.done();

		builder.event('cleaningStoppped')
			.description('Cleaning has stopped')
			.done();

		builder.action('cleaning')
			.description('Get the cleaning state')
			.returns('boolean', 'If thing is currently cleaning')
			.done();
	}

	constructor(...args) {
		super(...args);

		this.updateState('cleaning', false);
	}

	/**
	 * Get if thing is cleaning.
	 */
	cleaning() {
		return Promise.resolve(this.getState('cleaning'));
	}

	updateCleaning(cleaning) {
		cleaning = boolean(cleaning);

		if(cleaning) {
			// Assume error state has been cleared
			this.updateError(null);
		}

		if(this.updateState('cleaning', cleaning)) {
			this.emitEvent('cleaningChanged', cleaning);

			if(cleaning) {
				// Emit the cleaning started event
				this.emitEvent('cleaningStarted');
			} else {
				this.emitEvent('cleaningStopped');
			}
		}
	}
});
