'use strict';

const Thing = require('../thing');
const State = require('../common/state');
const { code } = require('../values');

/**
 * FanLevel capability, for appliances that support different fanLevels.
 */
module.exports = Thing.mixin(Parent => class extends Parent.with(State) {
	/**
	* Define the API of appliances that can manage their fanLevel.
	*/
	static availableAPI(builder) {
		builder.state('fanLevel')
			.type('string')
			.description('The current fanLevel of this thing')
			.done();

		builder.state('fanLevels')
			.type('array')
			.description('The available fanLevels of this thing')
			.done();

		builder.event('fanLevelChanged')
			.type('string')
			.description('The fanLevel of the thing has changed')
			.done();

		builder.event('fanLevelsChanged')
			.type('array')
			.description('The availables fanLevels of the thing have changed')
			.done();

		builder.action('fanLevel')
			.description('Get the fanLevel of this thing')
			.returns('fanLevel', 'The fanLevel of the thing')
			.getterForState('fanLevel')
			.done();

		builder.action('fanLevels')
			.description('Get the available fanLevels')
			.returns('array', 'The fanLevels that are supported')
			.getterForState('fanLevels')
			.done();
	}

	/**
	* Get that this provides the fanLevel capability.
	*/
	static get capability() {
		return 'fanLevel';
	}

	constructor(...args) {
		super(...args);

		this.updateState('fanLevel', null);
		this.updateState('fanLevels', []);
	}

	/**
	* Get or switch the fanLevel of the appliance.
	*
	* @returns
	*   string indicating the fanLevel
	*/
	fanLevel() {
		return Promise.resolve(this.getState('fanLevel'));
	}

	/**
	* Update the fanLevel of the appliance. Will emit events.
	*
	* @param {string} fanLevel
	*/
	updateFanLevel(fanLevel) {
		const fanLevels = this.getState('fanLevels');
		const fanLevelFound = fanLevels.find((fl) => {
			return fl.id === fanLevel;
		});
		fanLevel = fanLevelFound || code({
			id: fanLevel,
			description: 'Unknown fanLevel ' + fanLevel
		});
		if(this.updateState('fanLevel', fanLevel)) {
			this.emitEvent('fanLevelChanged', fanLevel);
		}
	}

	/**
	 * Get the available fanLevels of the device.
	 */
	fanLevels() {
		return Promise.resolve(this.getState('fanLevels'));
	}

	/**
	 * Get the available fanLevels of the device.
	 */
	updateFanLevels(fanLevels) {
		const mapped = [];
		for(const m of fanLevels) {
			mapped.push(code(m));
		}

		if(this.updateState('fanLevels', mapped)) {
			this.emitEvent('fanLevelsChanged', mapped);
		}
	}
});
