'use strict';

const Thing = require('../thing');
const State = require('../common/state');

/**
 * HorizontalSwing capability, for appliances that support switching and monitoring the
 * horizontalSwing status of themselves.
 */
module.exports = Thing.mixin(Parent => class extends Parent.with(State) {
	/**
	* Define the API of appliances that can manage their horizontalSwing.
	*/
	static availableAPI(builder) {
		builder.state('horizontalSwing')
			.type('boolean')
			.description('The horizontalSwing state of this appliance')
			.done();

		builder.event('horizontalSwingChanged')
			.type('boolean')
			.description('The horizontalSwing state of the appliance has changed')
			.done();

		builder.action('horizontalSwing')
			.description('Get the horizontalSwing state of this appliance')
			.returns('boolean', 'The horizontalSwing state of the appliance')
			.getterForState('horizontalSwing')
			.done();
	}

	/**
	* Get that this provides the horizontalSwing capability.
	*/
	static get capability() {
		return 'horizontal-swing';
	}

	constructor(...args) {
		super(...args);

		this.updateState('horizontalSwing', false);
	}

	/**
	* Get the horizontalSwing state of this appliance.
	*
	* @returns
	*   boolean indicating the horizontalSwing level
	*/
	horizontalSwing() {
		return Promise.resolve(this.getState('horizontalSwing'));
	}

	/**
	* Update the state of horizontalSwing to the appliance. Implementations should call
	* this whenever the horizontalSwing changes, either from an external event or when
	* changeHorizontalSwing is called.
	*
	* @param {boolean} horizontalSwing
	*/
	updateHorizontalSwing(horizontalSwing) {
		if(this.updateState('horizontalSwing', horizontalSwing)) {
			this.emitEvent('horizontalSwingChanged', horizontalSwing);
		}
	}

});
