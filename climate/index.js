'use strict';

module.exports.AirMonitor = require('./air-monitor');
module.exports.AirPurifier = require('./air-purifier');

module.exports.WaterPurifier = require('./water-purifier');

module.exports.Fan = require('./fan');

module.exports.Humidifier = require('./humidifier');
module.exports.Dehumidifier = require('./dehumidifier');

module.exports.Vacuum = require('./vacuum');

module.exports.TargetHumidity = require('./target-humidity');
module.exports.AdjustableTargetHumidity = require('./adjustable-target-humidity');

module.exports.FanSpeed = require('./fan-speed');
module.exports.AdjustableFanSpeed = require('./adjustable-fan-speed');

module.exports.FanLevel = require('./fan-level');
module.exports.SwitchableFanLevel = require('./switchable-fan-level');

module.exports.WaterLevel = require('./water-level');
module.exports.SwitchableWaterLevel = require('./switchable-water-level');

module.exports.CleaningState = require('./cleaning-state');
module.exports.AutonomousCleaning = require('./autonomous-cleaning');
module.exports.SpotCleaning = require('./spot-cleaning');

module.exports.Thermostat = require('./thermostat');
module.exports.Heater = require('./heater');
module.exports.Cooler = require('./cooler');

module.exports.TargetTemperature = require('./target-temperature');
module.exports.AdjustableTargetTemperature = require('./adjustable-target-temperature');

module.exports.VerticalSwing = require('./vertical-swing');
module.exports.SwitchableVerticalSwing = require('./switchable-vertical-swing');
module.exports.HorizontalSwing = require('./horizontal-swing');
module.exports.SwitchableHorizontalSwing = require('./switchable-horizontal-swing');
