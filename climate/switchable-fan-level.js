'use strict';

const Thing = require('../thing');
const FanLevel = require('./fan-level');

/**
 * Switchable fanLevel capability.
 */
module.exports = Thing.mixin(Parent => class extends Parent.with(FanLevel) {
	/**
	* Define the API of appliances that can manage their fanLevel.
	*/
	static availableAPI(builder) {
		builder.action('fanLevel')
			.description('Get or set the fanLevel of this appliance')
			.argument('string', true, 'Optional fanLevel to change to')
			.returns('fanLevel', 'The fanLevel of the appliance')
			.getterForState('fanLevel')
			.done();

		builder.action('setFanLevel')
			.description('Set the fanLevel of this appliance')
			.argument('string', true, 'FanLevel to change to')
			.returns('fanLevel', 'The fanLevel of the appliance')
			.done();
	}

	/**
	* Get that this provides the switchable capability.
	*/
	static get capability() {
		return 'switchable-fan-level';
	}

	constructor(...args) {
		super(...args);
	}

	/**
	* Get or switch the fanLevel of the appliance.
	*
	* @param {string} fanLevel
	*   optional fanLevel to switch to
	* @returns
	*   string indicating the fanLevel
	*/
	fanLevel(fanLevel=undefined) {
		if(typeof fanLevel !== 'undefined') {
			return this.setFanLevel(fanLevel);
		}

		return super.fanLevel();
	}

	/**
	* Set the fanLevel of this appliance.
	*
	* @param {string} fanLevel
	*/
	setFanLevel(fanLevel) {
		try {
			if(typeof fanLevel === 'undefined') throw new Error('FanLevel must be specified');
			const fanLevels = this.getState('fanLevels');
			const fanLevelFound = fanLevels.find((item) => {
				return item.id === fanLevel;
			});
			
			if (!fanLevelFound) throw new Error('The fanLevel is not supported');

			return Promise.resolve(this.changeFanLevel(fanLevelFound))
				.then(() => this.getState('fanLevel'));
		} catch(ex) {
			return Promise.reject(ex);
		}
	}

	/**
	* Change the current fanLevel of the device.
	*
	* @param {fanLevel} fanLevel
	*/
	changeFanLevel(fanLevel) {
		throw new Error('changeFanLevel has not been implemented');
	}
});
