'use strict';

const Thing = require('../thing');
const HorizontalSwing = require('./horizontal-swing');
const { boolean } = require('../values');

/**
 * Switchable capability, for appliances where the horizontalSwing can be switched on
 * or off.
 */
module.exports = Thing.mixin(Parent => class extends Parent.with(HorizontalSwing) {
	/**
	* Define the API of appliances that can manage their horizontalSwing.
	*/
	static availableAPI(builder) {
		builder.action('horizontalSwing')
			.description('Get or set the horizontalSwing state of this appliance')
			.argument('boolean', true, 'Optional horizontalSwing state to change to')
			.returns('boolean', 'The horizontalSwing state of the appliance')
			.getterForState('horizontalSwing')
			.done();

		builder.action('setHorizontalSwing')
			.description('Set the horizontalSwing state of this appliance')
			.argument('boolean', false, 'The horizontalSwing state to change to')
			.returns('boolean', 'The horizontalSwing state of the appliance')
			.done();
	}

	/**
	* Get that this provides the switchable capability.
	*/
	static get capability() {
		return 'switchable-horizontal-swing';
	}

	constructor(...args) {
		super(...args);
	}

	/**
	* Get or switch the horizontalSwing of the appliance.
	*
	* @param {boolean} horizontalSwing
	*   optional horizontalSwing level to switch to
	* @returns
	*   boolean indicating the horizontalSwing level
	*/
	horizontalSwing(horizontalSwing=undefined) {
		if(typeof horizontalSwing !== 'undefined') {
			// Call changeHorizontalSwing and then return the new horizontalSwing state
			return this.setHorizontalSwing(horizontalSwing);
		}

		return super.horizontalSwing();
	}

	/**
	 * Set the horizontalSwing of this appliance.
	 *
	 * @param {boolean} horizontalSwing
	 */
	setHorizontalSwing(horizontalSwing) {
		try {
			horizontalSwing = boolean(horizontalSwing);

			return Promise.resolve(this.changeHorizontalSwing(horizontalSwing))
				.then(() => this.horizontalSwing());
		} catch(ex) {
			return Promise.reject(ex);
		}
	}

	/**
	* Change the current horizontalSwing state of the appliance. This method can return
	* a Promise if the horizontalSwing switching is asynchronous.
	*
	* @param {boolean} horizontalSwing
	*/
	changeHorizontalSwing(horizontalSwing) {
		throw new Error('changeHorizontalSwing has not been implemented');
	}
});
