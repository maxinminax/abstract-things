'use strict';

const Thing = require('../thing');
const VerticalSwing = require('./vertical-swing');
const { boolean } = require('../values');

/**
 * Switchable capability, for appliances where the verticalSwing can be switched on
 * or off.
 */
module.exports = Thing.mixin(Parent => class extends Parent.with(VerticalSwing) {
	/**
	* Define the API of appliances that can manage their verticalSwing.
	*/
	static availableAPI(builder) {
		builder.action('verticalSwing')
			.description('Get or set the verticalSwing state of this appliance')
			.argument('boolean', true, 'Optional verticalSwing state to change to')
			.returns('boolean', 'The verticalSwing state of the appliance')
			.getterForState('verticalSwing')
			.done();

		builder.action('setVerticalSwing')
			.description('Set the verticalSwing state of this appliance')
			.argument('boolean', false, 'The verticalSwing state to change to')
			.returns('boolean', 'The verticalSwing state of the appliance')
			.done();
	}

	/**
	* Get that this provides the switchable capability.
	*/
	static get capability() {
		return 'switchable-vertical-swing';
	}

	constructor(...args) {
		super(...args);
	}

	/**
	* Get or switch the verticalSwing of the appliance.
	*
	* @param {boolean} verticalSwing
	*   optional verticalSwing level to switch to
	* @returns
	*   boolean indicating the verticalSwing level
	*/
	verticalSwing(verticalSwing=undefined) {
		if(typeof verticalSwing !== 'undefined') {
			// Call changeVerticalSwing and then return the new verticalSwing state
			return this.setVerticalSwing(verticalSwing);
		}

		return super.verticalSwing();
	}

	/**
	 * Set the verticalSwing of this appliance.
	 *
	 * @param {boolean} verticalSwing
	 */
	setVerticalSwing(verticalSwing) {
		try {
			verticalSwing = boolean(verticalSwing);

			return Promise.resolve(this.changeVerticalSwing(verticalSwing))
				.then(() => this.verticalSwing());
		} catch(ex) {
			return Promise.reject(ex);
		}
	}

	/**
	* Change the current verticalSwing state of the appliance. This method can return
	* a Promise if the verticalSwing switching is asynchronous.
	*
	* @param {boolean} verticalSwing
	*/
	changeVerticalSwing(verticalSwing) {
		throw new Error('changeVerticalSwing has not been implemented');
	}
});
