const Thing = require('../thing');
const WaterLevel = require('./water-level');

module.exports = Thing.mixin(Parent => class extends Parent.with(WaterLevel) {
  static availableAPI(builder) {
    builder.action('waterLevel')
      .description('Get or set the waterLevel of this appliance')
      .argument('string', true, 'Optional waterLevel to change to')
      .returns('waterLevel', 'The waterLevel of the appliance')
      .getterForState('waterLevel')
      .done();

    builder.action('setWaterLevel')
      .description('Set the waterLevel of this appliance')
      .argument('string', true, 'WaterLevel to change to')
      .returns('waterLevel', 'The waterLevel of the appliance')
      .done();
  }

  static get capability() {
    return 'switchable-water-level';
  }

  constructor(...args) {
    super(...args);
  }
  
  waterLevel(waterLevel=undefined) {
    if(typeof waterLevel !== 'undefined') {
      return this.setWaterLevel(waterLevel);
    }

    return super.waterLevel();
  }
  
  setWaterLevel(waterLevel) {
    try {
      if(typeof waterLevel === 'undefined') throw new Error('WaterLevel must be specified');
			const waterLevels = this.getState('waterLevels');
			const waterLevelFound = waterLevels.find((item) => {
				return item.id === waterLevel;
			});
			
			if (!waterLevelFound) throw new Error('The waterLevel is not supported');

      return Promise.resolve(this.changeWaterLevel(waterLevelFound))
        .then(() => this.getState('waterLevel'));
    } catch(ex) {
      return Promise.reject(ex);
    }
  }

  changeWaterLevel(waterLevel) {
    throw new Error('changeWaterLevel has not been implemented');
  }
});