'use strict';

const isDeepEqual = require('deep-equal');
const Thing = require('../thing');
const State = require('../common/state');
const { temperature } = require('../values');
const targetTemperatureRange = Symbol('colorTemperatureRange');

module.exports = Thing.mixin(
	(Parent) =>
		class extends Parent.with(State) {
			static get capability() {
				return 'target-temperature';
			}

			static availableAPI(builder) {
				builder
					.event('targetTemperatureChanged')
					.type('temperature')
					.description('The target temperature has changed')
					.done();

				builder
					.action('targetTemperature')
					.description('Get the target temperature')
					.returns('temperature', 'The target temperature')
					.done();

				builder
					.action('targetTemperatureRange')
					.description('Get the temperature range supports')
					.returns('object')
					.done();

				builder
					.event('targetTemperatureRangeChanged')
					.type('object')
					.description('The supported target temperature range has changed')
					.done();
			}

			targetTemperature() {
				return Promise.resolve(this.getState('targetTemperature'));
			}

			updateTargetTemperature(target) {
				target = temperature(target);

				if (this.updateState('targetTemperature', target)) {
					this.emitEvent('targetTemperatureChanged', target);
				}
			}

			targetTemperatureRange() {
				const range = this[targetTemperatureRange];

				if (!range) {
					return Promise.reject(
						new Error('Temperature range has not been set')
					);
				}

				return Promise.resolve({
					min: range.min,
					max: range.max,
				});
			}

			updateTargetTemperatureRange(min, max) {
				min = temperature(min);
				max = temperature(max);

				if (min > max) {
					const temp = max;
					max = min;
					min = temp;
				}

				const range = { min, max };
				if (!isDeepEqual(this[targetTemperatureRange], range)) {
					this[targetTemperatureRange] = range;
					this.emitEvent('targetTemperatureRangeChanged', range);
				}
			}
		}
);
