'use strict';

const Thing = require('../thing');
const State = require('../common/state');

/**
 * VerticalSwing capability, for appliances that support switching and monitoring the
 * verticalSwing status of themselves.
 */
module.exports = Thing.mixin(Parent => class extends Parent.with(State) {
	/**
	* Define the API of appliances that can manage their verticalSwing.
	*/
	static availableAPI(builder) {
		builder.state('verticalSwing')
			.type('boolean')
			.description('The verticalSwing state of this appliance')
			.done();

		builder.event('verticalSwingChanged')
			.type('boolean')
			.description('The verticalSwing state of the appliance has changed')
			.done();

		builder.action('verticalSwing')
			.description('Get the verticalSwing state of this appliance')
			.returns('boolean', 'The verticalSwing state of the appliance')
			.getterForState('verticalSwing')
			.done();
	}

	/**
	* Get that this provides the verticalSwing capability.
	*/
	static get capability() {
		return 'vertical-swing';
	}

	constructor(...args) {
		super(...args);

		this.updateState('verticalSwing', false);
	}

	/**
	* Get the verticalSwing state of this appliance.
	*
	* @returns
	*   boolean indicating the verticalSwing level
	*/
	verticalSwing() {
		return Promise.resolve(this.getState('verticalSwing'));
	}

	/**
	* Update the state of verticalSwing to the appliance. Implementations should call
	* this whenever the verticalSwing changes, either from an external event or when
	* changeVerticalSwing is called.
	*
	* @param {boolean} verticalSwing
	*/
	updateVerticalSwing(verticalSwing) {
		if(this.updateState('verticalSwing', verticalSwing)) {
			this.emitEvent('verticalSwingChanged', verticalSwing);
		}
	}

});
