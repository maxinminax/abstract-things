const Thing = require('../thing');
const State = require('../common/state');
const { code } = require('../values');

module.exports = Thing.mixin(Parent => class extends Parent.with(State) {
  static availableAPI(builder) {
    builder.state('waterLevel')
      .type('string')
      .description('The current waterLevel of this thing')
      .done();

    builder.state('waterLevels')
      .type('array')
      .description('The available waterLevels of this thing')
      .done();

    builder.event('waterLevelChanged')
      .type('string')
      .description('The waterLevel of the thing has changed')
      .done();

    builder.event('waterLevelsChanged')
      .type('array')
      .description('The availables waterLevels of the thing have changed')
      .done();

    builder.action('waterLevel')
      .description('Get the waterLevel of this thing')
      .returns('waterLevel', 'The waterLevel of the thing')
      .getterForState('waterLevel')
      .done();

    builder.action('waterLevels')
      .description('Get the available waterLevels')
      .returns('array', 'The waterLevels that are supported')
      .getterForState('waterLevels')
      .done();
  }

  /**
	* Get that this provides the waterLevel capability.
	*/
  static get capability() {
    return 'water-level';
  }

  constructor(...args) {
    super(...args);

    this.updateState('waterLevel', null);
    this.updateState('waterLevels', []);
  }
  
  waterLevel() {
    return Promise.resolve(this.getState('waterLevel'));
  }

  updateWaterLevel(waterLevel) {
		const waterLevels = this.getState('waterLevels');
		const waterLevelFound = waterLevels.find((wl) => {
			return wl.id === waterLevel;
		});
		waterLevel = waterLevelFound || code({
			id: waterLevel,
			description: 'Unknown waterLevel ' + waterLevel
		});
    if(this.updateState('waterLevel', waterLevel)) {
      this.emitEvent('waterLevelChanged', waterLevel);
    }
  }
  
  waterLevels() {
    return Promise.resolve(this.getState('waterLevels'));
  }

  updateWaterLevels(waterLevels) {
    const mapped = [];
    for(const m of waterLevels) {
      mapped.push(code(m));
    }

    if(this.updateState('waterLevels', mapped)) {
      this.emitEvent('waterLevelsChanged', mapped);
    }
  }
});