'use strict';

const Thing = require('../thing');

/**
 * Water Purifier.
 */
module.exports = Thing.type(Parent => class extends Parent {
	static get type() {
		return 'water-purifier';
	}
});
