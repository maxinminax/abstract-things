'use strict';

const Thing = require('../thing');
const Position = require('./position');
const { percentage } = require('../values');

module.exports = Thing.mixin(Parent => class extends Parent.with(Position) {

	static get capability() {
		return 'adjustable-position';
	}

	static availableAPI(builder) {
		builder.action('position')
			.description('Get or set the position')
			.argument('percentage', true, 'Optional position to set')
			.returns('percentage', 'The position')
			.done();

		builder.action('setPosition')
			.description('Set the position of this appliance')
			.argument('percentage', true, 'Position to change to')
			.returns('percentage', 'The position of the appliance')
			.done();

		builder.action('open')
			.description('Open')
			.done();

		builder.action('close')
			.description('Close')
			.done();

		builder.action('stop')
			.description('Stop')
			.done();
	}

	position(speed) {
		if(typeof speed === 'undefined') {
			return super.position();
		}

		return this.setPosition(speed);
	}

	setPosition(speed) {
		speed = percentage(speed, true);

		try {
			return Promise.resolve(this.changePosition(speed))
				.then(() => super.position());
		} catch(ex) {
			return Promise.reject(ex);
		}
	}

	changePosition(speed) {
		throw new Error('changePosition not implemented');
	}

	open() {
		try {
			return Promise.resolve(this.adjustMaxPosition())
				.then(() => null);
		} catch(ex) {
			return Promise.reject(ex);
		}
	}

	close() {
		try {
			return Promise.resolve(this.adjustMinPosition())
				.then(() => null);
		} catch(ex) {
			return Promise.reject(ex);
		}
	}

	stop() {
		try {
			return Promise.resolve(this.stopAdjustPosition())
				.then(() => null);
		} catch(ex) {
			return Promise.reject(ex);
		}
	}

	adjustMaxPosition() {
		throw new Error('adjustMaxPosition not implemented');
	}

	adjustMinPosition() {
		throw new Error('adjustMinPosition not implemented');
	}

	stopAdjustPosition() {
		throw new Error('stopAdjustPosition not implemented');
	}
});
