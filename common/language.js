'use strict';

const Thing = require('../thing');
const State = require('./state');
const { code } = require('../values');

/**
 * Language capability, for appliances that support different languages.
 */
module.exports = Thing.mixin(Parent => class extends Parent.with(State) {
	/**
	* Define the API of appliances that can manage their power.
	*/
	static availableAPI(builder) {
		builder.state('language')
			.type('string')
			.description('The current language of this thing')
			.done();

		builder.state('languages')
			.type('array')
			.description('The available languages of this thing')
			.done();

		builder.event('languageChanged')
			.type('string')
			.description('The language of the thing has changed')
			.done();

		builder.event('languagesChanged')
			.type('array')
			.description('The availables languages of the thing have changed')
			.done();

		builder.action('language')
			.description('Get the language of this thing')
			.returns('language', 'The language of the thing')
			.getterForState('language')
			.done();

		builder.action('languages')
			.description('Get the available languages')
			.returns('array', 'The languages that are supported')
			.getterForState('languages')
			.done();
	}

	/**
	* Get that this provides the language capability.
	*/
	static get capability() {
		return 'language';
	}

	constructor(...args) {
		super(...args);

		this.updateState('language', null);
		this.updateState('languages', []);
	}

	/**
	* Get or switch the language of the appliance.
	*
	* @returns
	*   string indicating the language
	*/
	language() {
		return Promise.resolve(this.getState('language'));
	}

	/**
	* Update the language of the appliance. Will emit events.
	*
	* @param {string} language
	*/
	updateLanguage(language) {
		const languages = this.getState('languages');
		const languageFound = languages.find((m) => {
			return m.id === language;
		});
		language = languageFound || code({
			id: language,
			description: 'Unknown language ' + language
		});
		if(this.updateState('language', language)) {
			this.emitEvent('languageChanged', language);
		}
	}

	/**
	 * Get the available languages of the device.
	 */
	languages() {
		return Promise.resolve(this.getState('languages'));
	}

	/**
	 * Get the available languages of the device.
	 */
	updateLanguages(languages) {
		const mapped = [];
		for(const m of languages) {
			mapped.push(code(m));
		}

		if(this.updateState('languages', mapped)) {
			this.emitEvent('languagesChanged', mapped);
		}
	}
});
