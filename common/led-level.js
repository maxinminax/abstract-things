'use strict';

const Thing = require('../thing');
const State = require('./state');
const { code } = require('../values');

/**
 * LedLevel capability, for appliances that support different ledLevels.
 */
module.exports = Thing.mixin(Parent => class extends Parent.with(State) {
	/**
	* Define the API of appliances that can manage their power.
	*/
	static availableAPI(builder) {
		builder.state('ledLevel')
			.type('string')
			.description('The current ledLevel of this thing')
			.done();

		builder.state('ledLevels')
			.type('array')
			.description('The available ledLevels of this thing')
			.done();

		builder.event('ledLevelChanged')
			.type('string')
			.description('The ledLevel of the thing has changed')
			.done();

		builder.event('ledLevelsChanged')
			.type('array')
			.description('The availables ledLevels of the thing have changed')
			.done();

		builder.action('ledLevel')
			.description('Get the ledLevel of this thing')
			.returns('ledLevel', 'The ledLevel of the thing')
			.getterForState('ledLevel')
			.done();

		builder.action('ledLevels')
			.description('Get the available ledLevels')
			.returns('array', 'The ledLevels that are supported')
			.getterForState('ledLevels')
			.done();
	}

	/**
	* Get that this provides the ledLevel capability.
	*/
	static get capability() {
		return 'led-level';
	}

	constructor(...args) {
		super(...args);

		this.updateState('ledLevel', null);
		this.updateState('ledLevels', []);
	}

	/**
	* Get or switch the ledLevel of the appliance.
	*
	* @returns
	*   string indicating the ledLevel
	*/
	ledLevel() {
		return Promise.resolve(this.getState('ledLevel'));
	}

	/**
	* Update the ledLevel of the appliance. Will emit events.
	*
	* @param {string} ledLevel
	*/
	updateLedLevel(ledLevel) {
		const ledLevels = this.getState('ledLevels');
		const ledLevelFound = ledLevels.find((l) => {
			return l.id === ledLevel;
		});
		ledLevel = ledLevelFound || code({
			id: ledLevel,
			description: 'Unknown ledLevel ' + ledLevels
		});
		if(this.updateState('ledLevel', ledLevel)) {
			this.emitEvent('ledLevelChanged', ledLevel);
		}
	}

	/**
	 * Get the available ledLevels of the device.
	 */
	ledLevels() {
		return Promise.resolve(this.getState('ledLevels'));
	}

	/**
	 * Get the available ledLevels of the device.
	 */
	updateLedLevels(ledLevels) {
		const mapped = [];
		for(const m of ledLevels) {
			mapped.push(code(m));
		}

		if(this.updateState('ledLevels', mapped)) {
			this.emitEvent('ledLevelsChanged', mapped);
		}
	}
});
