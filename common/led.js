'use strict';

const Thing = require('../thing');
const State = require('./state');

/**
 * Led capability, for appliances that support switching and monitoring the
 * led status of themselves.
 */
module.exports = Thing.mixin(Parent => class extends Parent.with(State) {
	/**
	* Define the API of appliances that can manage their led.
	*/
	static availableAPI(builder) {
		builder.state('led')
			.type('boolean')
			.description('The led state of this appliance')
			.done();

		builder.event('ledChanged')
			.type('boolean')
			.description('The led state of the appliance has changed')
			.done();

		builder.action('led')
			.description('Get the led state of this appliance')
			.returns('boolean', 'The led state of the appliance')
			.getterForState('led')
			.done();
	}

	/**
	* Get that this provides the led capability.
	*/
	static get capability() {
		return 'led';
	}

	constructor(...args) {
		super(...args);

		this.updateState('led', false);
	}

	/**
	* Get the led state of this appliance.
	*
	* @returns
	*   boolean indicating the led level
	*/
	led() {
		return Promise.resolve(this.getState('led'));
	}

	/**
	* Update the state of led to the appliance. Implementations should call
	* this whenever the led changes, either from an external event or when
	* changeLed is called.
	*
	* @param {boolean} led
	*/
	updateLed(led) {
		if(this.updateState('led', led)) {
			this.emitEvent('ledChanged', led);
		}
	}

});
