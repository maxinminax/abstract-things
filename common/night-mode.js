'use strict';

const Thing = require('../thing');
const State = require('./state');

/**
 * NightMode capability, for appliances that support switching and monitoring the
 * nightMode status of themselves.
 */
module.exports = Thing.mixin(Parent => class extends Parent.with(State) {
	/**
	* Define the API of appliances that can manage their nightMode.
	*/
	static availableAPI(builder) {
		builder.state('nightMode')
			.type('boolean')
			.description('The nightMode state of this appliance')
			.done();

		builder.event('nightModeChanged')
			.type('boolean')
			.description('The nightMode state of the appliance has changed')
			.done();

		builder.action('nightMode')
			.description('Get the nightMode state of this appliance')
			.returns('boolean', 'The nightMode state of the appliance')
			.getterForState('nightMode')
			.done();
	}

	/**
	* Get that this provides the nightMode capability.
	*/
	static get capability() {
		return 'night-mode';
	}

	constructor(...args) {
		super(...args);

		this.updateState('nightMode', false);
	}

	/**
	* Get the nightMode state of this appliance.
	*
	* @returns
	*   boolean indicating the nightMode level
	*/
	nightMode() {
		return Promise.resolve(this.getState('nightMode'));
	}

	/**
	* Update the state of nightMode to the appliance. Implementations should call
	* this whenever the nightMode changes, either from an external event or when
	* changeNightMode is called.
	*
	* @param {boolean} nightMode
	*/
	updateNightMode(nightMode) {
		if(this.updateState('nightMode', nightMode)) {
			this.emitEvent('nightModeChanged', nightMode);
		}
	}

});
