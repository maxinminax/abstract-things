'use strict';

const Thing = require('../thing');
const State = require('./state');
const { percentage } = require('../values');

module.exports = Thing.mixin(Parent => class extends Parent.with(State) {

	static availableAPI(builder) {
		builder.state('position')
			.type('percentage')
			.description('Current battery level of the appliance')
			.done();

		builder.event('positionChanged')
			.type('percentage')
			.description('Battery level of the appliance has changed')
			.done();

		builder.action('position')
			.description('Get the battery level of the appliance')
			.returns('percentage', 'Current battery level')
			.done();
	}

	static get capability() {
		return 'position';
	}

	constructor(...args) {
		super(...args);

		this.updateState('position', -1);
	}

	position() {
		return Promise.resolve(this.getState('position'));
	}

	updatePosition(level) {
		level = percentage(level);
		if(this.updateState('position', level)) {
			this.emitEvent('positionChanged', level);
		}
	}
});
