'use strict';

const Thing = require('../thing');
const State = require('./state');
const { code } = require('../values');

/**
 * Status capability, for appliances that support different statuses.
 */
module.exports = Thing.mixin(Parent => class extends Parent.with(State) {
	/**
	* Define the API of appliances that can manage their power.
	*/
	static availableAPI(builder) {
		builder.state('status')
			.type('string')
			.description('The current status of this thing')
			.done();

		builder.state('statuses')
			.type('array')
			.description('The available statuses of this thing')
			.done();

		builder.event('statusChanged')
			.type('string')
			.description('The status of the thing has changed')
			.done();

		builder.event('statusesChanged')
			.type('array')
			.description('The availables statuses of the thing have changed')
			.done();

		builder.action('status')
			.description('Get the status of this thing')
			.returns('status', 'The status of the thing')
			.getterForState('status')
			.done();

		builder.action('statuses')
			.description('Get the available statuses')
			.returns('array', 'The statuses that are supported')
			.getterForState('statuses')
			.done();
	}

	/**
	* Get that this provides the status capability.
	*/
	static get capability() {
		return 'status';
	}

	constructor(...args) {
		super(...args);

		this.updateState('status', null);
		this.updateState('statuses', []);
	}

	/**
	* Get or switch the status of the appliance.
	*
	* @returns
	*   string indicating the status
	*/
	status() {
		return Promise.resolve(this.getState('status'));
	}

	/**
	* Update the status of the appliance. Will emit events.
	*
	* @param {string} status
	*/
	updateStatus(status) {
		const statuses = this.getState('statuses');
		const statusFound = statuses.find((s) => {
			return s.id === status;
    });
		status = statusFound || code({
			id: status,
			description: 'Unknown status ' + status
    });
    
		if(this.updateState('status', status)) {
			this.emitEvent('statusChanged', status);
		}
	}

	/**
	 * Get the available statuses of the device.
	 */
	statuses() {
		return Promise.resolve(this.getState('statuses'));
	}

	/**
	 * Get the available statuses of the device.
	 */
	updateStatuses(statuses) {
		const mapped = [];
		for(const s of statuses) {
			mapped.push(code(s));
		}

		if(this.updateState('statuses', mapped)) {
			this.emitEvent('statusesChanged', mapped);
		}
	}
});
