'use strict';

const Thing = require('../thing');
const Language = require('./language');

/**
 * Switchable language capability.
 */
module.exports = Thing.mixin(Parent => class extends Parent.with(Language) {
	/**
	* Define the API of appliances that can manage their language.
	*/
	static availableAPI(builder) {
		builder.action('language')
			.description('Get or set the language of this appliance')
			.argument('string', true, 'Optional language to change to')
			.returns('language', 'The language of the appliance')
			.getterForState('language')
			.done();

		builder.action('setLanguage')
			.description('Set the language of this appliance')
			.argument('string', true, 'Language to change to')
			.returns('language', 'The language of the appliance')
			.done();
	}

	/**
	* Get that this provides the switchable capability.
	*/
	static get capability() {
		return 'switchable-language';
	}

	constructor(...args) {
		super(...args);
	}

	/**
	* Get or switch the language of the appliance.
	*
	* @param {string} language
	*   optional language to switch to
	* @returns
	*   string indicating the language
	*/
	language(language=undefined) {
		if(typeof language !== 'undefined') {
			return this.setLanguage(language);
		}

		return super.language();
	}

	/**
	* Set the language of this appliance.
	*
	* @param {string} language
	*/
	setLanguage(language) {
		try {
			if(typeof language === 'undefined') throw new Error('Language must be specified');
			language = String(language);

			return Promise.resolve(this.changeLanguage(language))
				.then(() => this.getState('language'));
		} catch(ex) {
			return Promise.reject(ex);
		}
	}

	/**
	* Change the current language of the device.
	*
	* @param {language} language
	*/
	changeLanguage(language) {
		throw new Error('changeLanguage has not been implemented');
	}
});
