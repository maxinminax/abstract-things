'use strict';

const Thing = require('../thing');
const LedLevel = require('./led-level');

/**
 * Switchable ledLevel capability.
 */
module.exports = Thing.mixin(Parent => class extends Parent.with(LedLevel) {
	/**
	* Define the API of appliances that can manage their ledLevel.
	*/
	static availableAPI(builder) {
		builder.action('ledLevel')
			.description('Get or set the ledLevel of this appliance')
			.argument('string', true, 'Optional ledLevel to change to')
			.returns('ledLevel', 'The ledLevel of the appliance')
			.getterForState('ledLevel')
			.done();

		builder.action('setLedLevel')
			.description('Set the ledLevel of this appliance')
			.argument('string', true, 'LedLevel to change to')
			.returns('ledLevel', 'The ledLevel of the appliance')
			.done();
	}

	/**
	* Get that this provides the switchable capability.
	*/
	static get capability() {
		return 'switchable-led-level';
	}

	constructor(...args) {
		super(...args);
	}

	/**
	* Get or switch the ledLevel of the appliance.
	*
	* @param {string} ledLevel
	*   optional ledLevel to switch to
	* @returns
	*   string indicating the ledLevel
	*/
	ledLevel(ledLevel=undefined) {
		if(typeof ledLevel !== 'undefined') {
			return this.setLedLevel(ledLevel);
		}

		return super.ledLevel();
	}

	/**
	* Set the ledLevel of this appliance.
	*
	* @param {string} ledLevel
	*/
	setLedLevel(ledLevel) {
		try {
			if(typeof ledLevel === 'undefined') throw new Error('LedLevel must be specified');
			const ledLevels = this.getState('ledLevels');
			const ledLevelFound = ledLevels.find((item) => {
				return item.id === ledLevel;
			});
			
			if (!ledLevelFound) throw new Error('The ledLevel is not supported');

			return Promise.resolve(this.changeLedLevel(ledLevelFound))
				.then(() => this.getState('ledLevel'));
		} catch(ex) {
			return Promise.reject(ex);
		}
	}

	/**
	* Change the current ledLevel of the device.
	*
	* @param {ledLevel} ledLevel
	*/
	changeLedLevel(ledLevel) {
		throw new Error('changeLedLevel has not been implemented');
	}
});
