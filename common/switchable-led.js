'use strict';

const Thing = require('../thing');
const Led = require('./led');
const RestorableState = require('./restorable-state');
const { boolean } = require('../values');

/**
 * Switchable capability, for appliances where the led can be switched on
 * or off.
 */
module.exports = Thing.mixin(Parent => class extends Parent.with(Led, RestorableState) {
	/**
	* Define the API of appliances that can manage their led.
	*/
	static availableAPI(builder) {
		builder.action('led')
			.description('Get or set the led state of this appliance')
			.argument('boolean', true, 'Optional led state to change to')
			.returns('boolean', 'The led state of the appliance')
			.getterForState('led')
			.done();

		builder.action('setLed')
			.description('Set the led state of this appliance')
			.argument('boolean', false, 'The led state to change to')
			.returns('boolean', 'The led state of the appliance')
			.done();
	}

	/**
	* Get that this provides the switchable capability.
	*/
	static get capability() {
		return 'switchable-led';
	}

	constructor(...args) {
		super(...args);
	}

	/**
	* Get or switch the led of the appliance.
	*
	* @param {boolean} led
	*   optional led level to switch to
	* @returns
	*   boolean indicating the led level
	*/
	led(led=undefined) {
		if(typeof led !== 'undefined') {
			// Call changeLed and then return the new led state
			return this.setLed(led);
		}

		return super.led();
	}

	/**
	 * Set the led of this appliance.
	 *
	 * @param {boolean} led
	 */
	setLed(led) {
		try {
			led = boolean(led);

			return Promise.resolve(this.changeLed(led))
				.then(() => this.led());
		} catch(ex) {
			return Promise.reject(ex);
		}
	}

	/**
	* Change the current led state of the appliance. This method can return
	* a Promise if the led switching is asynchronous.
	*
	* @param {boolean} led
	*/
	changeLed(led) {
		throw new Error('changeLed has not been implemented');
	}

	/**
	 * Perform a led change from a state restore. Delegates to `changeLed`
	 * but is provided to support smarter state management such as for lights
	 * where this is handled by a custom `setLightState`.
	 *
	 * @param {} led
	 */
	changeLedState(led) {
		return this.changeLed(led);
	}

	get restorableState() {
		return [ ...super.restorableState, 'led' ];
	}

	changeState(state) {
		return super.changeState(state)
			.then(() => {
				if(typeof state.led !== 'undefined') {
					return this.changeLedState(state.led);
				}
			});
	}
});
