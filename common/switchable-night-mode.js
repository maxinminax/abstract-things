'use strict';

const Thing = require('../thing');
const NightMode = require('./night-mode');
const { boolean } = require('../values');

/**
 * Switchable capability, for appliances where the nightMode can be switched on
 * or off.
 */
module.exports = Thing.mixin(Parent => class extends Parent.with(NightMode) {
	/**
	* Define the API of appliances that can manage their nightMode.
	*/
	static availableAPI(builder) {
		builder.action('nightMode')
			.description('Get or set the nightMode state of this appliance')
			.argument('boolean', true, 'Optional nightMode state to change to')
			.returns('boolean', 'The nightMode state of the appliance')
			.getterForState('nightMode')
			.done();

		builder.action('setNightMode')
			.description('Set the nightMode state of this appliance')
			.argument('boolean', false, 'The nightMode state to change to')
			.returns('boolean', 'The nightMode state of the appliance')
			.done();
	}

	/**
	* Get that this provides the switchable capability.
	*/
	static get capability() {
		return 'switchable-night-mode';
	}

	constructor(...args) {
		super(...args);
	}

	/**
	* Get or switch the nightMode of the appliance.
	*
	* @param {boolean} nightMode
	*   optional nightMode level to switch to
	* @returns
	*   boolean indicating the nightMode level
	*/
	nightMode(nightMode=undefined) {
		if(typeof nightMode !== 'undefined') {
			// Call changeNightMode and then return the new nightMode state
			return this.setNightMode(nightMode);
		}

		return super.nightMode();
	}

	/**
	 * Set the nightMode of this appliance.
	 *
	 * @param {boolean} nightMode
	 */
	setNightMode(nightMode) {
		try {
			nightMode = boolean(nightMode);

			return Promise.resolve(this.changeNightMode(nightMode))
				.then(() => this.nightMode());
		} catch(ex) {
			return Promise.reject(ex);
		}
	}

	/**
	* Change the current nightMode state of the appliance. This method can return
	* a Promise if the nightMode switching is asynchronous.
	*
	* @param {boolean} nightMode
	*/
	changeNightMode(nightMode) {
		throw new Error('changeNightMode has not been implemented');
	}
});
