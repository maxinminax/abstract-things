'use strict';

const Thing = require('../thing');
const Sensor = require('./sensor');
const { temperature } = require('../values');

module.exports = Thing.mixin(Parent => class extends Parent.with(Sensor) {
	static get capability() {
		return 'relative-temperature';
	}

	static availableAPI(builder) {
		builder.event('relativeTemperatureChanged')
			.type('percentage')
			.description('Current relative temperature has changed')
			.done();

		builder.action('relativeTemperature')
			.description('Get the current relative temperature')
			.getterForState('relativeTemperature')
			.returns('percentage', 'Current relative temperature')
			.done();
	}

	get sensorTypes() {
		return [ ...super.sensorTypes, 'relativeTemperature' ];
	}

	relativeTemperature() {
		return this.value('relativeTemperature');
	}

	updateRelativeTemperature(value) {
		this.updateValue('relativeTemperature', temperature(value));
	}
});
