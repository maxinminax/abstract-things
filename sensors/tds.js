'use strict';

const Thing = require('../thing');
const Sensor = require('./sensor');
const { number } = require('../values');

module.exports = Thing.mixin(Parent => class extends Parent.with(Sensor) {
	static get capability() {
		return 'tds';
	}

	static availableAPI(builder) {
		builder.event('tdsChanged')
			.type('number')
			.description('TDS density has changed')
			.done();

		builder.action('tds')
			.description('Get the current TDS density')
			.getterForState('tds')
			.returns('number', 'Current TDS density')
			.done();
	}

	get sensorTypes() {
		return [ ...super.sensorTypes, 'tds' ];
	}

	tds() {
		return this.value('tds');
	}

	updateTDS(value) {
		this.updateValue('tds', number(value));
	}
});
